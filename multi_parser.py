# coding=utf-8
from threading import Thread, Event, Lock
from math import ceil
import requests
import re


class Parser:
    url = 'http://tmclass.tmdn.org/ec2/term/'
    proxy_url = 'https://api.best-proxies.ru/proxylist.txt?key=211d7ff04b9a2027541053061c754b67&type=https&level=1' \
                '&limit=0 '
    agents = []
    proxies = []
    errors_proxy = []
    total_page = 0
    total_proxy = 0
    progress_parse_page = 0
    progress_check_proxy = 0
    status_event_page = False

    def __init__(self, process):
        self.process = process
        self.status_event = Event()

        with open('agents.txt', encoding='utf8') as f:
            for line in f:
                self.agents.append({'User-Agent': line.strip()})

    def run(self, lists):
        Thread(target=self.print_status).start()
        self.parser_proxy()
        if len(self.proxies):
            self.total_page = len(lists)
            self.status_event_page = True
            if self.total_page < self.process:
                lists = [
                    {'index': i, 'agent': self.agents[i % len(self.agents)],
                     'proxy': self.proxies[i % len(self.proxies)]}
                    for i in lists]

                self.multi_processing(lists, self.parser_page)
            else:
                n = ceil(self.total_page / self.process)
                for lst in [lists[i::n] for i in range(n)]:
                    if len(self.proxies) < 100:
                        self.status_event_page = False
                        self.parser_proxy()
                        self.status_event_page = True

                    lst = [
                        {'index': i, 'agent': self.agents[i % len(self.agents)],
                         'proxy': self.proxies[i % len(self.proxies)]}
                        for i in lst]

                    self.multi_processing(lst, self.parser_page)

            self.status_event.set()
            self.progress_parse_page = 0
            print('\nПарсинг данных завершен')
        else:
            print('Не удалось парсить прокси. Попробуйте позже')

    def parser_page(self, index, lock):
        page = self.get_page(self.url + str(index['index']), index['agent'], index['proxy'])
        if page is None:
            self.delete_proxy(index['proxy'])

            lock.acquire()
            try:
                with open('failed_pages.txt', 'a', encoding='utf8') as f:
                    f.write(str(index['index']) + '\n')
            finally:
                lock.release()
        else:
            res = re.search(r'<h2>.*</h2>', page)
            if res is not None:
                with open('pages_success/{}.html'.format(index['index']), 'w', encoding='utf8') as f:
                    f.write(page)
            else:
                res = re.search(r'<.+error-name.+>', page)
                if res is not None:
                    self.delete_proxy(index['proxy'])
                    
                    lock.acquire()
                    try:
                        with open('failed_pages.txt', 'a', encoding='utf8') as f:
                            f.write(str(index['index']) + '\n')
                    finally:
                        lock.release()
                else:
                    with open('pages_error/{}.txt'.format(index['index']), 'w', encoding='utf8') as f:
                        f.write(page)

        self.progress_parse_page += 1

    def delete_proxy(self, proxy):
        try:
            self.proxies.remove(proxy)
        except ValueError:
            self.errors_proxy.append(proxy)

    def parser_proxy(self):
        page = self.get_page(self.proxy_url)
        proxies = page.split('\r\n')
        self.total_proxy = len(proxies)
        if self.total_proxy < self.process:
            self.multi_processing(proxies, self.check_proxy)
        else:
            n = ceil(self.total_proxy / self.process)
            for proxy in [proxies[i::n] for i in range(n)]:
                self.multi_processing(proxy, self.check_proxy)

        self.progress_check_proxy = 0
        print('')

    def check_proxy(self, proxy, lock):
        try:
            page = self.get_page(self.url + '1', self.agents[0], {'http': proxy})
            if page:
                res = re.search(r'<h2>.*</h2>', page)

                if res is not None:
                    self.proxies.append({'http': proxy})
        except requests.RequestException:
            pass

        self.progress_check_proxy += 1

    @staticmethod
    def get_page(url, agent=None, proxy=None, time_out=10):
        try:
            if agent and proxy:
                response = requests.get(url, headers=agent, proxies=proxy, timeout=time_out)
            else:
                response = requests.get(url)
            return response.text
        except Exception:
            return None

    @staticmethod
    def multi_processing(lst, callback):
        lock = Lock()
        threads = []
        for data in lst:
            t = Thread(target=callback, args=(data, lock))
            threads.append(t)
            t.start()

        for thread in threads:
            thread.join()

    def print_status(self):
        loader = ['-', '\\', '|', '/']
        i = 0
        while not self.status_event.wait(0.2):
            if self.status_event_page:
                print(
                    '{} Парсинг данных: {} из {}\r'.format(
                        loader[i],
                        self.progress_parse_page,
                        self.total_page),
                    end='')
            else:
                print(
                    '{} Проверка прокси: {} из {}. Проверку прошли {}\r'.format(
                        loader[i],
                        self.progress_check_proxy,
                        self.total_proxy,
                        len(self.proxies)),
                    end='')
            i += 1
            if i == 4:
                i = 0
