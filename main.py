# coding=utf-8
from multi_parser import Parser
from time import time
from math import ceil
import sys
import os


def main(start, last, process, update=False):
    start_parsing = time()

    def get_failed_pages():
        tmp = []
        if os.path.exists('failed_pages.txt'):
            with open('failed_pages.txt') as f:
                for line in f:
                    tmp.append(int(line.strip()))
        return tmp

    def get_max_page(dir_name):
        max_num = 0
        files = os.listdir(dir_name)
        for file in files:
            num = int(file.split('.')[0])
            if num > max_num:
                max_num = num
        return max_num

    if update:
        lists = get_failed_pages()
        if len(lists):
            os.remove('failed_pages.txt')
        else:
            raise Exception('Нет списка страниц для парсинга')
    else:
        max_page = 0
        if os.path.exists('pages_success'):
            max_page = get_max_page('pages_success')
        else:
            os.mkdir('pages_success')

        if os.path.exists('pages_error'):
            error = get_max_page('pages_error')
            max_page = error if error > max_page else max_page
        else:
            os.mkdir('pages_error')

        failed = get_failed_pages()
        if len(failed):
            failed = max(failed)
        max_page = failed if failed > max_page else max_page
        if max_page:
            start = max_page + 1

        print('Начальная страница', start)
        if start > last:
            raise Exception('Начальная страница больше или равно конечной страницы')
        else:
            lists = [i for i in range(start, last + 1)]

    parser = Parser(process)
    parser.run(lists)

    end_parsing = ceil(time() - start_parsing)
    days = str(end_parsing // 86400) + ' дней ' if (end_parsing // 86400) else ''
    hours = str(end_parsing // 3600 % 24) + ' часов ' if (end_parsing // 3600 % 24) else ''
    minute = str(end_parsing // 60 % 60) + ' минут ' if (end_parsing // 60 % 60) else ''
    print('Прошло времени: {}{}{}{} сек'.format(days, hours, minute, end_parsing % 60))


if __name__ == '__main__':
    START = 1
    LAST = 1000000
    PROCESS = 1000

    try:
        if len(sys.argv) > 1 and sys.argv[1] == 'update':
            main(START, LAST, PROCESS, True)
        else:
            main(START, LAST, PROCESS)
    except Exception as e:
        print(e)
